<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org"
	xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout"
	layout:decorate="~{fragments/main_layout}">
<head>
<title>Register Form</title>
</head>
<body>
<div class="form-group ${status.error ? 'has-error' : ''}">
		<form:form method="post" action="/register" modelAttribute="user">
				<table>
					<tr>
						<td><form:label path="name">Name</form:label></td>
						<td><form:input path="name"/></td>
					</tr>
					<tr>
						<td><form:label path="password">pass</form:label></td>
						<td><form:input type="password" path="password"/></td>
						<form:errors path="password"></form:errors>
					</tr>
					<tr>
						<td><form:label path="email">
							</form:label>email</td>
						<td><form:input path="email"/></td>
					</tr>
					<tr>
						<td><input type="submit" value="Submit"/></td>
					</tr>
				</table>
		</form:form>
</body>
</html>