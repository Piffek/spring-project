<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns:th="http://www.thymeleaf.org"
	xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout"
	layout:decorate="~{fragments/main_layout}">
<head>
<title>Index Page</title>
</head>
<body>

	<div layout:fragment="content" class="container mySpace">
		<c:forEach items="${users}">
			<b>${users.username}</b>
		</c:forEach>
	</div>
</body>
</html>