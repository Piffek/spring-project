package com.example.piwkofirst.dao;

import com.example.piwkofirst.model.FlashCard;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class FlashCardDao implements Dao<FlashCard> {

    @PersistenceContext
    private EntityManager em;


    @Override
    public void save(FlashCard flashCard) {
        em.persist(flashCard);
    }

    @Override
    public FlashCard getById(long id) {
        FlashCard flashcard = em.find(FlashCard.class, id);
        return flashcard;
    }

    @Override
    public void delete(long id) {
        FlashCard flashCard = em.find(FlashCard.class, id);

        em.remove(flashCard);
    }

    @Override
    public void update(FlashCard flashCard) {
        em.persist(flashCard);
    }

    @Override
    public List<FlashCard> getAll() {
        String sql = "select t from FlashCard t";
        List<FlashCard> allFlashCard = em.createQuery("select t from FlashCard t", FlashCard.class).getResultList();

        return allFlashCard;
    }
}
