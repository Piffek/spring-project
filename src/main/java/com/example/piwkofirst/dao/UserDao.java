package com.example.piwkofirst.dao;

import com.example.piwkofirst.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserDao  extends JpaRepository<User, String> {

    List<User> findByNameLike(String name);

}
