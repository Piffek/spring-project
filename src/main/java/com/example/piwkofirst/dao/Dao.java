package com.example.piwkofirst.dao;

import java.util.List;

public interface Dao<T> {
    public void save(T t);
    T getById(long id);
    public void delete(long id);
    public void update(T t);
    public List<T> getAll();
}