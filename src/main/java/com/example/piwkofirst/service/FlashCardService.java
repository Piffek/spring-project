package com.example.piwkofirst.service;

import com.example.piwkofirst.dao.Dao;
import com.example.piwkofirst.model.FlashCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlashCardService {

    @Autowired
    private Dao<FlashCard> flashCardDao;

    public void saveFlashCard(FlashCard flashCard) {
        try{
            flashCardDao.save(flashCard);
        }catch (DataAccessException e) {
            e.printStackTrace();
        }
    }

    public List<FlashCard> getAllFlashCard() {
        try {
            return flashCardDao.getAll();
        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

    public FlashCard getFlashCardById(long id) {
        try {
            return flashCardDao.getById(id);
        } catch(DataAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void deleteFlashCard(long id) {
        try {
            flashCardDao.delete(id);
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
    }

    public void updateFlashCard(FlashCard flashCard) {
        try {
            flashCardDao.update(flashCard);
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
    }
}
