package com.example.piwkofirst.controller;

import com.example.piwkofirst.model.User;
import com.example.piwkofirst.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;
import java.util.List;

@Controller
public class ProfileController {

    @Autowired
    private UserService userService;

    @GetMapping("/profile")
    public String showProfilePage(Model model, Principal principal) {

        String email = principal.getName();
        List<User> users = userService.findAll();
        model.addAttribute("currentUserEmail", email);

        return "views/list";
    }
}