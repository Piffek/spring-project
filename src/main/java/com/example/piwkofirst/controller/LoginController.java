package com.example.piwkofirst.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
class LoginController {


    @GetMapping("/")
    public String showIndexPage() {

        return "index";
    }

    @GetMapping("/login")
    public String showLoginForm() {

        return "views/loginForm";
    }





}
