package com.example.piwkofirst.controller;

import com.example.piwkofirst.configuration.security.validators.RegisterValidator;
import com.example.piwkofirst.model.User;
import com.example.piwkofirst.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
class RegisterController {
    @Autowired
    private UserService userService;

    @Autowired
    RegisterValidator registerValidator;

    @GetMapping("/register")
    public ModelAndView registerForm(Model model) {

        return new ModelAndView("views/registerForm", "user", new User());
    }

    @PostMapping("/register")
    public String registerUser(@ModelAttribute("user") User user, BindingResult bindingResult, Model model) {

        //to repair
        registerValidator.validate(user, bindingResult);

        if(bindingResult.hasErrors()) {
            return "views/registerForm";
        }

        userService.createUser(user);

        return "views/success";
    }
}