package com.example.piwkofirst.configuration.security.validators;


import com.example.piwkofirst.model.User;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class RegisterValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        User user = (User) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty", "NotEmpty.user.name");
        if(user.getPassword().length() < 6) {
            errors.reject("email", "Size.user.password");
        }
    }
}
